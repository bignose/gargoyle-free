gargoyle-free (2019.1.1-2) unstable; urgency=medium

  * Fix FTBFS with gcc-10 (Closes: #957253)
  * Drop unneeded quilt dependency
  * Font unbundling: replace jam patch with jam variable
  * Verbose build
  * Only build in parallel if requested through DEB_BUILD_OPTIONS
  * desktop_menu_entry.patch: forwarded upstream

 -- Sylvain Beucler <beuc@debian.org>  Thu, 14 May 2020 22:11:04 +0200

gargoyle-free (2019.1.1-1) unstable; urgency=medium

  [ Stephen Kitt ]

  * debian/control: move URLs to Salsa

  [ Sylvain Beucler]

  * New upstream release (Closes: #850096, #957253)
  * DFSG repackage
  * Update copyright information, documentation, patches
  * Allow specifying a game to run in .desktop entry (Closes: #882768)
  * Actually install alan2
  * Don't install garglk.ini as example since it's already installed in /etc
  * debian/watch: update naming convention
  * Bump standards to 4.5.0 (no changes)
  * Bump debhelper to 12; drop explicit --parallel

 -- Sylvain Beucler <beuc@debian.org>  Thu, 14 May 2020 14:43:40 +0200

gargoyle-free (2011.1b-1) unstable; urgency=medium

  * Team upload.
  * Repacked upstream release, dropping the fonts (Closes: #851111).

 -- Stephen Kitt <skitt@debian.org>  Wed, 25 Jan 2017 08:59:03 +0100

gargoyle-free (2011.1a-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with GCC6 by using -std=gnu++98 (Closes: #812058).

 -- Andrey Rahmatullin <wrar@debian.org>  Sat, 03 Dec 2016 01:38:34 +0500

gargoyle-free (2011.1a-3) unstable; urgency=medium

  [ Alexandre Detiste ]
  * Update project url in debian/copyright and debian/watch
  * Bump debhelper compat level to 9
  * Use canonical vcs url
  * debian/TODO: Alan 2 is included since 2011.1a-1

  [ Sylvain Beucler ]
  * Replace Matthias' patch with finer one from upstream tracker
  * Bump standards to 3.9.6 (no changes)

 -- Sylvain Beucler <beuc@debian.org>  Wed, 15 Jul 2015 14:12:02 +0200

gargoyle-free (2011.1a-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix build failure with GCC 5. Closes: #777860.
  * Build-depend on libpng-dev instead of libpng12-dev. Closes: #662341.

 -- Matthias Klose <doko@debian.org>  Wed, 08 Jul 2015 14:42:50 +0200

gargoyle-free (2011.1a-2) unstable; urgency=low

  * Fix package description: Alan 2 is now included.

 -- Sylvain Beucler <beuc@debian.org>  Sun, 13 Oct 2013 17:08:18 +0200

gargoyle-free (2011.1a-1) unstable; urgency=low

  * New upstream release
  * Alan 2 interpreter is now Free Software, include it
  * Update fonts package names in dependencies (Closes: #715160)
  * Bump Standards-Version to 3.9.4

 -- Sylvain Beucler <beuc@debian.org>  Sun, 28 Jul 2013 13:38:56 +0200

gargoyle-free (2011.1-2) unstable; urgency=low

  * Fix FTBFS: sdl-sound1.2-1.0.3-6 stripped its depencies, and the
    gargoyle build system made unnecessary references to -lsmpeg and
    -lvorbis that are normally used when building with the bundled copy of
    sdl-sound. (Closes: #674336)
  * Fix FTBFS: -lrt was missing. This has never been a problem so I
    suspect we previously got it indirectly through pkg-config --libs.
  * Bump Standards-Version to 3.9.3
  * Minimize ignore_bundled_libraries.patch.

 -- Sylvain Beucler <beuc@debian.org>  Thu, 24 May 2012 22:57:48 +0200

gargoyle-free (2011.1-1) unstable; urgency=low

  * New upstream release
  * Alan 3 interpreter is now Free Software, include it
  * Add pkg-config as a direct build dependency
  * Upgrade packaging to debhelper 7 and source format 3.0 (quilt)
  * Remove helper-templates-in-copyright override as the lintian check is
    smarter now
  * Refresh patches
  * Update debian/copyright

 -- Sylvain Beucler <beuc@debian.org>  Sun, 19 Feb 2012 15:57:30 +0100

gargoyle-free (2010.1-2) unstable; urgency=low

  * Depend on libjpeg-dev rather than libjpeg62-dev (Closes: #633939)
  Cf. http://lists.debian.org/debian-devel-announce/2010/02/msg00006.html
  Thanks to Bill Allombert
  * Bump Standards-Version to 3.9.2
  * Ignore helper-templates-in-copyright due to the presence of
    "author(s)" in the license text

 -- Sylvain Beucler <beuc@debian.org>  Sat, 23 Jul 2011 00:02:01 +0200

gargoyle-free (2010.1-1) experimental; urgency=low

  * New upstream release
  * Update debian/copyright
  * Adjust font dependencies to use the new default fonts (Liberation Mono
    and Linux Libertine O instead of DejaVu Sans)
  * Build convenience library statically
  * Embed the text of BSD in debian/copyright (lintian
    copyright-refers-to-deprecated-bsd-license-file)
  * Bump Standards-Version
  * Use the googlecode.debian.net redirector in debian/watch
  * Hard-code lookup path to interpreters in /usr/lib/gargoyle/ (rather
    than in the executable's directory - following upstream's advice)
  * Remove patches merged upstream or obsoleted
  * Install new desktop menu entry and icon

 -- Sylvain Beucler <beuc@debian.org>  Fri, 01 Oct 2010 20:28:01 +0200

gargoyle-free (2009-08-25-2) unstable; urgency=high

  * Fix security issue in the shell wrapper, where LD_LIBRARY_PATH may be
    modified to include an empty directory (which means "current
    directory")
  * Fix missing 'not' in the package description: "it does not provide a
    way to display the cover art"
  * Fix download URL in debian/copyright
  * Fix a couple typos
  * Fix FTBFS with binutils-gold: specify -lm explicitly in the build
    system (patch sent upstream) (Closes: #554390)
  * Provide: zcode-interpreter, tads2-interpreter, tads3-interpreter as
    other packaged interpreters do (Closes: #579618)
  * Remove comments from the watch file because taste differs among DDs
  * Bump Standards-Version
  * Declare package source format (1.0)

 -- Sylvain Beucler <beuc@debian.org>  Thu, 03 Jun 2010 15:59:02 +0200

gargoyle-free (2009-08-25-1) unstable; urgency=low

  * Initial release (Closes: #546217)
  * Drop the non-free Alan and Hugo interpreters, as well as the non-free
    Luxi Mono monospace font, from the upstream tarball
  * Replace Luxi Mono with DejaVu Sans Mono
  * Load GPL-incompatible DejaVu Sans Mono font from an external file (located
    using libfontconfig), instead of bundling it in the GPL executable
  * Link to SDL_sound dynamically (instead of statically)
  * Install and locate auxiliary binaries in /usr/lib/gargoyle/

 -- Sylvain Beucler <beuc@beuc.net>  Fri, 11 Sep 2009 20:09:43 +0200
